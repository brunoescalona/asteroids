package com.score.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;

public class ScoreServer {

    public static final String SOCKET_SCORES = "PUNTUACIONES";
    public static final String SOCKET_SAVED_SCORES_OK = "OK";

    public static void main(String args[]){

        Vector<String> scoresList = new Vector<>();

        try{
            ServerSocket serverSocket = new ServerSocket(1234);
            System.out.println("Esperando conexiones...");

            while(true){
                Socket client = serverSocket.accept();
                BufferedReader input = new BufferedReader(
                        new InputStreamReader(client.getInputStream()));
                PrintWriter output = new PrintWriter(
                        new OutputStreamWriter(client.getOutputStream()), true);
                String data = input.readLine();
                if(data.equals(SOCKET_SCORES)){
                    for(int i = 0; i<scoresList.size();i++){
                        output.println(scoresList.get(i));
                    }
                }else{
                    scoresList.add(0, data);
                    output.println(SOCKET_SAVED_SCORES_OK);
                }
                client.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}

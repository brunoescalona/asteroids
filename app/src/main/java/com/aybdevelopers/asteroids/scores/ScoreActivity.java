package com.aybdevelopers.asteroids.scores;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.aybdevelopers.asteroids.MainActivity;
import com.aybdevelopers.asteroids.R;

/**
 * Created by AYB on 09/10/2016.
 */

public class ScoreActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ScoreAdapter mScoreAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mScoreAdapter = new ScoreAdapter(this, MainActivity.mScoresStore);
        mRecyclerView.setAdapter(mScoreAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }
}

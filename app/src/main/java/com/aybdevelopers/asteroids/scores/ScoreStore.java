package com.aybdevelopers.asteroids.scores;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.StrictMode;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;

import com.aybdevelopers.asteroids.R;
import com.aybdevelopers.asteroids.preferences.SharedPreferencesAsteroids;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by AYB on 21/11/2016.
 */

public class ScoreStore implements ScoresStore {


    private static final String PREFERENCES_POINTS = "points";
    private static final String POINT = "point";
    private Context context;
    private PointsList listPointsXml;

    private boolean loadedList;

    private String storeType;

    private static final String FILE_POINTS = "points.txt";
    private static final String FILE_EXTERNAL_POINTS = Environment.getExternalStorageDirectory() + "/points.txt";
    private static final String FILE_POINTS_XML = "points.xml";
    private static final String IP_SOCKET_STORAGE = "158.42.146.127";

    private static final String BASE_IP = "http://158.42.146.127";
    private static final String ENDPOINT_GET_SCORES = "/puntuaciones/lista.php?max=20";
    private static final String ENDPOINT_POST_SCORES = "/puntuaciones/nueva.php";

    public ScoreStore(Context context, String storeType) {
        this.context = context;
        this.storeType = storeType;
        this.loadedList = false;
        this.listPointsXml = new PointsList();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());
    }

    @Override
    public void saveScore(int score, String name, long date) {
        switch (storeType){
            case SharedPreferencesAsteroids.STORE_PREFERENCES:
                saveScorePreferences(score, name, date);
                break;
            case SharedPreferencesAsteroids.STORE_FILE:
                saveScoreFile(score, name, date);
                break;
            case SharedPreferencesAsteroids.STORE_FILE_EXTERNAL:
                saveScoreFileExternal(score, name, date);
                break;
            case SharedPreferencesAsteroids.STORE_FILE_XML_SAX:
                saveScoreFileXMLSAX(score, name, date);
                break;
            case SharedPreferencesAsteroids.STORE_DATABASE_LOCAL:
                saveScoreDataBaseLocal(score, name, date);
                break;
            case SharedPreferencesAsteroids.STORE_SOCKET:
                saveScoreSocket(score, name, date);
                break;
            case SharedPreferencesAsteroids.STORE_WEB_SERVICE:
                saveScoreWebService(score, name, date);
                break;
            default:
                break;
        }
    }

    @Override
    public Vector<String> scoresList(int numberScores) {
        switch (storeType){
            case SharedPreferencesAsteroids.STORE_PREFERENCES:
                return scoreListPreferences(numberScores);
            case SharedPreferencesAsteroids.STORE_FILE:
                return scoreListFile(numberScores);
            case SharedPreferencesAsteroids.STORE_FILE_EXTERNAL:
                return scoreListFileExternal(numberScores);
            case SharedPreferencesAsteroids.STORE_FILE_RAW:
                return scoreListFileRaw(numberScores);
            case SharedPreferencesAsteroids.STORE_FILE_ASSETS:
                return scoreListFileAssets(numberScores);
            case SharedPreferencesAsteroids.STORE_FILE_XML_SAX:
                return scoreListFileXMLSAX(numberScores);
            case SharedPreferencesAsteroids.STORE_DATABASE_LOCAL:
                return scoreListDataBaseLocal(numberScores);
            case SharedPreferencesAsteroids.STORE_SOCKET:
                return scoreListSocket(numberScores);
            case SharedPreferencesAsteroids.STORE_WEB_SERVICE:
                return scoreListWebService(numberScores);
            default:
                return null;
        }
    }

    private void saveScorePreferences(int score, String name, long date){
        SharedPreferences mPreferences = context.getSharedPreferences(PREFERENCES_POINTS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mPreferences.edit();
        for (int i = 9; i>=1 ; i--){
            editor.putString(POINT + i, mPreferences.getString(POINT + (i-1),""));
        }
        editor.putString(POINT +0, score + " " + name );

        editor.commit();
    }

    private Vector<String> scoreListPreferences(int numberScores){
        Vector<String> result = new Vector<>();
        SharedPreferences mPreferences = context.getSharedPreferences(PREFERENCES_POINTS, Context.MODE_PRIVATE);
        for(int i = 0; i<=9;i++){
            String resultString =  mPreferences.getString(POINT + i, "");
            if(resultString!=""){
                result.add(resultString);
            }
        }

        return result;
    }

    private void saveScoreSocket(int score, String name, long date){
        try{
            Socket socket = new Socket(IP_SOCKET_STORAGE, 1234);
            BufferedReader input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            PrintWriter output = new PrintWriter(
                    new OutputStreamWriter(socket.getOutputStream()), true);
            output.println(score + " " + name);
            String response = input.readLine();
            if(!response.equals("OK")){
                Log.e("Asteroids", "ERROR: wrong server response");
            }
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Vector<String> scoreListSocket(int numberScores){
        Vector<String> result = new Vector<>();

        try{
            Socket socket = new Socket(IP_SOCKET_STORAGE, 1234);
            BufferedReader input = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            PrintWriter output = new PrintWriter(
                    new OutputStreamWriter(socket.getOutputStream()), true);
            output.println("PUNTUACIONES");
            int i = 0;
            String response;
            do{
                response = input.readLine();
                if(response!=null){
                    result.add(response);
                    i++;
                }
            }while(i<numberScores && response!=null);
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private void saveScoreWebService(int score, String name, long date){

        HttpURLConnection connection;

        try {

            URL url = new URL(BASE_IP + ENDPOINT_POST_SCORES + "?" +
            "puntos=" + score +
            "&nombre=" + name +
            "&fecha=" + date);

            connection = (HttpURLConnection) url.openConnection();

            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader input = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String line = input.readLine();
               if(!line.equals("OK")){
                   Log.e("Asteroids", "Error en servicio web nueva");
               }
            }
            connection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Vector<String> scoreListWebService(int numberScores){
        Vector<String> result = new Vector<>();
        HttpURLConnection connection;
        try {
            URL url = new URL(BASE_IP + ENDPOINT_GET_SCORES);
            connection = (HttpURLConnection) url.openConnection();

            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                BufferedReader input = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String line = input.readLine();
                while(!line.equals("")){
                    result.add(line);
                    line = input.readLine();
                }
                input.close();
                connection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private void saveScoreDataBaseLocal(int score, String name, long date){
        ScoreDataBase scoreDataBase = new ScoreDataBase(context);
        SQLiteDatabase dataBase = scoreDataBase.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ScoreDataBase.TABLE_POINTS, score);
        values.put(ScoreDataBase.TABLE_USER, name);
        values.put(ScoreDataBase.TABLE_DATE, date);

        dataBase.insert(ScoreDataBase.TABLE_NAME, null, values);
        dataBase.close();
    }

    private Vector<String> scoreListDataBaseLocal(int numberScores){
        Vector<String> result = new Vector<>();
        ScoreDataBase scoreDataBase = new ScoreDataBase(context);
        SQLiteDatabase dataBase = scoreDataBase.getReadableDatabase();

        /*Cursor cursor = dataBase.rawQuery("SELECT " + ScoreDataBase.TABLE_POINTS + ", " + ScoreDataBase.TABLE_USER +
            " FROM " + ScoreDataBase.TABLE_NAME + " ORDER BY " + ScoreDataBase.TABLE_POINTS + " DESC LIMIT " +
            numberScores, null);*/

        String[] projection = {ScoreDataBase.TABLE_POINTS, ScoreDataBase.TABLE_USER, ScoreDataBase.TABLE_DATE};

        Cursor cursor = dataBase.query(
                ScoreDataBase.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                ScoreDataBase.TABLE_POINTS + " DESC",
                Integer.toString(numberScores)
        );
        while(cursor.moveToNext()){
            result.add(cursor.getInt(0)+ " " + cursor.getString(1));
        }
        cursor.close();
        dataBase.close();
        return result;
    }

    private void saveScoreFile(int score, String name, long date){
        try{
            FileOutputStream file = context.openFileOutput(FILE_POINTS, Context.MODE_APPEND);
            String text = score + " " + name + "\n";
            file.write(text.getBytes());
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Vector<String> scoreListFile(int numberScores){
        Vector<String> result = new Vector<>();
        try{
            FileInputStream file = context.openFileInput(FILE_POINTS);
            BufferedReader enter = new BufferedReader(new InputStreamReader((file)));
            int i = 0;
            String line;
            do{
                line = enter.readLine();
                if(line != null){
                    result.add(0,line);
                    i++;
                }
            } while(i<numberScores && line != null);
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private void saveScoreFileExternal(int score, String name, long date){
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            Toast.makeText(context, "No se puede escribir en la memoria externa", Toast.LENGTH_LONG).show();
            return;
        }
        try{
            FileOutputStream file = new FileOutputStream(FILE_EXTERNAL_POINTS, true);
            String text = score + " " + name + "\n";
            file.write(text.getBytes());
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Vector<String> scoreListFileExternal(int numberScores){
        Vector<String> result = new Vector<>();
        if(!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) &&
                !Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY)){
            Toast.makeText(context, "No se puede leer en la memoria externa", Toast.LENGTH_LONG).show();
            return result;
        }
        try{
            FileInputStream file = new FileInputStream(FILE_EXTERNAL_POINTS);
            BufferedReader enter = new BufferedReader(new InputStreamReader((file)));
            int i = 0;
            String line;
            do{
                line = enter.readLine();
                if(line != null){
                    result.add(0,line);
                    i++;
                }
            } while(i<numberScores && line != null);
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private Vector<String> scoreListFileRaw(int numberScores){
        Vector<String> result = new Vector<>();
        try{
            InputStream file = context.getResources().openRawResource(R.raw.points);
            BufferedReader enter = new BufferedReader(new InputStreamReader((file)));
            int i = 0;
            String line;
            do{
                line = enter.readLine();
                if(line != null){
                    result.add(0,line);
                    i++;
                }
            } while(i<numberScores && line != null);
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private Vector<String> scoreListFileAssets(int numberScores){
        Vector<String> result = new Vector<>();
        try{
            InputStream file = context.getAssets().open("points.txt");
            BufferedReader enter = new BufferedReader(new InputStreamReader((file)));
            int i = 0;
            String line;
            do{
                line = enter.readLine();
                if(line != null){
                    result.add(0,line);
                    i++;
                }
            } while(i<numberScores && line != null);
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }


    private void saveScoreFileXMLSAX(int score, String name, long date){
        try{
            if(!loadedList){
                listPointsXml.readXML(context.openFileInput(FILE_POINTS_XML));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        listPointsXml.addPoints(score, name, date);
        try{
            listPointsXml.writeXML(context.openFileOutput(FILE_POINTS_XML, Context.MODE_PRIVATE));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Vector<String> scoreListFileXMLSAX(int numberScores){
        try{
            if(!loadedList){
                listPointsXml.readXML(context.openFileInput(FILE_POINTS_XML));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listPointsXml.pointsToVectorString();
    }


    private class PointsList{

        private class Points{
            int point;
            String name;
            long date;
        }

        private List<Points> pointsList;

        public PointsList() {
            this.pointsList = new ArrayList<>();
        }

        public void addPoints(int point, String name, long date){
            Points pointsValue = new Points();
            pointsValue.point = point;
            pointsValue.name = name;
            pointsValue.date = date;
            pointsList.add(pointsValue);
        }

        public Vector<String> pointsToVectorString(){
            Vector<String> result = new Vector<>();
            for(Points points:pointsList){
                result.add(points.point + " " + points.name);
            }
            return result;
        }

        public void readXML(InputStream input) throws Exception{
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            XMLReader reader = parser.getXMLReader();
            HandlerXML handler = new HandlerXML();
            reader.setContentHandler(handler);
            reader.parse(new InputSource(input));
            loadedList = true;
        }

        public void writeXML(OutputStream output){
            XmlSerializer serializer = Xml.newSerializer();
            try{
                serializer.setOutput(output, "UTF-8");
                serializer.startDocument("UTF-8", true);
                serializer.startTag("", "list_scores");
                for(Points points:pointsList){
                    serializer.startTag("","score");
                    serializer.attribute("", "date", String.valueOf(points.date));
                    serializer.startTag("","name");
                    serializer.text(points.name);
                    serializer.endTag("","name");
                    serializer.startTag("","point");
                    serializer.text(String.valueOf(points.point));
                    serializer.endTag("","point");
                    serializer.endTag("","score");
                }
                serializer.endTag("", "list_scores");
                serializer.endDocument();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private class HandlerXML extends DefaultHandler{
            private StringBuilder cadena;
            private Points points;

            @Override
            public void startDocument() throws SAXException {
                pointsList = new ArrayList<>();
                cadena = new StringBuilder();
            }

            @Override
            public void startElement(String uri, String nombreLocal, String
                    nombreCualif, Attributes atr) throws SAXException {
                cadena.setLength(0);
                if (nombreLocal.equals("score")) {
                    points = new Points();
                    points.date = Long.parseLong(atr.getValue("date"));
                }
            }
            @Override
            public void characters(char ch[], int comienzo, int lon) {
                cadena.append(ch, comienzo, lon);
            }

            @Override
            public void endElement(String uri, String nombreLocal,
                                   String nombreCualif) throws SAXException {
                if (nombreLocal.equals("point")) {
                    points.point = Integer.parseInt(cadena.toString());
                } else if (nombreLocal.equals("name")) {
                    points.name = cadena.toString();
                } else if (nombreLocal.equals("score")) {
                    pointsList.add(0,points);
                }
            }

            @Override
            public void endDocument() throws SAXException {

            }
        }
    }


}



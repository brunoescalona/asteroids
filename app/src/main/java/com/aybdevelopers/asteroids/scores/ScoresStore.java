package com.aybdevelopers.asteroids.scores;

import java.util.Vector;

/**
 * Created by AYB on 09/10/2016.
 */

public interface ScoresStore {
    void saveScore(int score, String name, long date);

    Vector<String> scoresList(int numberScores);
}

package com.aybdevelopers.asteroids.scores;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by AYB on 21/11/2016.
 */

public class ScoreDataBase extends SQLiteOpenHelper {

    static final String DATABASE_NAME = "scores.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_NAME = "scores";
    public static final String TABLE_ID = "_id";
    public static final String TABLE_POINTS = "point";
    public static final String TABLE_USER = "name";
    public static final String TABLE_DATE= "date";

    public ScoreDataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_SCORES_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +

                TABLE_ID + " INTEGER PRIMARY KEY UNIQUE, " +
                TABLE_POINTS + " INTEGER, " +
                TABLE_USER + " TEXT, " +
                TABLE_DATE + " LONG)";

        db.execSQL(SQL_CREATE_SCORES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

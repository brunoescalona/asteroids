package com.aybdevelopers.asteroids.scores;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aybdevelopers.asteroids.R;

import java.util.Vector;

/**
 * Created by AYB on 09/10/2016.
 */

public class ScoreAdapter extends RecyclerView.Adapter<ScoreAdapter.ViewHolder> {

    protected Context mContext;
    protected Vector<String> mScoreVector;
    protected LayoutInflater mLayoutInflater;

    public ScoreAdapter(Context context, ScoreStore scoreStoreArray) {
        this.mContext = context;
        this.mScoreVector = scoreStoreArray.scoresList(10);
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.item_score_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(mContext, mTextView.getText(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = mLayoutInflater.inflate(R.layout.item_score, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mTextView.setText(mScoreVector.get(position));
    }

    @Override
    public int getItemCount() {
        if(mScoreVector==null){
            return 0;
        }else{
            return mScoreVector.size();
        }

    }

}

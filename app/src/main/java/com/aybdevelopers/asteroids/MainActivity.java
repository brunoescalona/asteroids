package com.aybdevelopers.asteroids;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.aybdevelopers.asteroids.about.AboutActivity;
import com.aybdevelopers.asteroids.play.PlayActivity;
import com.aybdevelopers.asteroids.preferences.PreferencesActivity;
import com.aybdevelopers.asteroids.preferences.SharedPreferencesAsteroids;
import com.aybdevelopers.asteroids.scores.ScoreActivity;
import com.aybdevelopers.asteroids.scores.ScoreStore;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int CODE_POINTS = 1234;

    public static ScoreStore mScoresStore;

    private TextView textViewTitle;
    private Button buttonAbout;
    private Button buttonConfiguration;
    private Button buttonPlay;
    private Button buttonScore;

    private MediaPlayer mediaPlayer;
    private SharedPreferencesAsteroids mSharedPreferencesAsteroids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewTitle = (TextView) findViewById(R.id.main_title);
        buttonAbout = (Button) findViewById(R.id.button_about);
        buttonAbout.setOnClickListener(this);
        buttonConfiguration = (Button) findViewById(R.id.button_configuration);
        buttonConfiguration.setOnClickListener(this);
        buttonPlay = (Button) findViewById(R.id.button_play);
        buttonPlay.setOnClickListener(this);
        buttonScore = (Button) findViewById(R.id.button_score);
        buttonScore.setOnClickListener(this);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.rotation);
        textViewTitle.startAnimation(animation);
        Animation animationTranslateRight = AnimationUtils.loadAnimation(this, R.anim.displacement_from_right);
        buttonAbout.startAnimation(animationTranslateRight);
        buttonPlay.startAnimation(animationTranslateRight);
        Animation animationTranslateLeft = AnimationUtils.loadAnimation(this, R.anim.displacement_from_left);
        buttonConfiguration.startAnimation(animationTranslateLeft);
        buttonScore.startAnimation(animationTranslateLeft);

        mSharedPreferencesAsteroids = new SharedPreferencesAsteroids(this);
        mediaPlayer = MediaPlayer.create(this, R.raw.background_audio);
        mediaPlayer.setLooping(true);

        mScoresStore = new ScoreStore(this,mSharedPreferencesAsteroids.getStoreType());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSharedPreferencesAsteroids.getMusicActive()) {
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mSharedPreferencesAsteroids.getMusicActive()) {
            mediaPlayer.start();
        }
        mScoresStore = new ScoreStore(this,mSharedPreferencesAsteroids.getStoreType());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mediaPlayer != null) {
            int position = mediaPlayer.getCurrentPosition();
            outState.putInt("position", position);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && mediaPlayer != null) {
            int position = savedInstanceState.getInt("position");
            mediaPlayer.seekTo(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_asteroids, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_about_item:
                startAboutActivity(null);
                return true;
            case R.id.menu_preferences_item:
                startPreferenceActivity(null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {

        final String PLAY = "play";
        final String CONFIGURATION = "configuration";
        final String ABOUT = "about";
        final String SCORE = "score";

        switch (view.getTag().toString()) {
            case PLAY:
                startPlayActivity(null);
                break;
            case CONFIGURATION:
                startPreferenceActivity(null);
                break;
            case ABOUT:
                startAboutActivity(null);
                break;
            case SCORE:
                startScoreActivity(null);
                break;
            default:
                break;
        }
    }

    private void startAboutActivity(View view) {
        Intent mIntent = new Intent(this, AboutActivity.class);
        startActivity(mIntent);
    }

    private void startPreferenceActivity(View view) {
        Intent mIntent = new Intent(this, PreferencesActivity.class);
        startActivity(mIntent);
    }

    private void startScoreActivity(View view) {
        Intent mIntent = new Intent(this, ScoreActivity.class);
        startActivity(mIntent);
    }

    private void startPlayActivity(View view) {
        Intent mIntent = new Intent(this, PlayActivity.class);
        startActivityForResult(mIntent, CODE_POINTS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(CODE_POINTS==requestCode && resultCode==RESULT_OK && data!=null){
            int points = data.getExtras().getInt(PlayActivity.EXTRA_POINTS);

            //TODO introducir una ventana para meter el nombre de usuario
            String name = "yo";

            mScoresStore.saveScore(points,name,System.currentTimeMillis());
            startScoreActivity(null);
        }
    }
}

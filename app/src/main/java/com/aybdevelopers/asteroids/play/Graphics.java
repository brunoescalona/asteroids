package com.aybdevelopers.asteroids.play;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

/**
 * Created by AYB on 09/10/2016.
 */

public class Graphics {

    private Drawable mDrawable;         //Image to draw
    private int centerX, centerY;       //Center of the graphic
    private int width, height;            //Dimensions
    private double speedX, speedY;      //Speed
    private double angle, rotation;     //Angle and rotation speed
    private int collisionRatio;         //Collision ratio
    private int xPrevious, yPrevious;   //Previous center values
    private int radioInvalidate;        //ratio used in invalidate()
    private View mView;                  //Used in view.invalidate()

    public Graphics(View view, Drawable drawable){
        this.mView = view;
        this.mDrawable = drawable;
        this.width = mDrawable.getIntrinsicWidth();
        this.height = mDrawable.getIntrinsicHeight();
        collisionRatio = (width+height)/4;
        radioInvalidate = (int) (Math.hypot(width/2,height/2));
    }


    public void drawGraphic(Canvas canvas){
        int x = centerX - width/2;
        int y = centerY - height/2;
        mDrawable.setBounds(x,y,x+width,y+height);
        canvas.save();
        canvas.rotate((float)angle, centerX, centerY);
        mDrawable.draw(canvas);
        canvas.restore();
        mView.invalidate(centerX-radioInvalidate,centerY-radioInvalidate,
                centerX+radioInvalidate,centerY+radioInvalidate);
        mView.invalidate(xPrevious-radioInvalidate,yPrevious-radioInvalidate,
                xPrevious+radioInvalidate,yPrevious+radioInvalidate);
        xPrevious = centerX;
        yPrevious = centerY;
    }

    public void incrementPosition(double factor){
        centerX += speedX*factor;
        centerY += speedY*factor;
        angle += rotation*factor;
        if(centerX<0) centerX = mView.getWidth();
        if(centerX>mView.getWidth()) centerX = 0;
        if(centerY<0) centerY = mView.getHeight();
        if(centerY>mView.getHeight()) centerY = 0;
    }

    public double getDistance(Graphics graphics){
        return Math.hypot(centerX-graphics.centerX,centerY-graphics.centerY);
    }

    public boolean onCollisionEnter(Graphics graphics){
        return(getDistance(graphics)<(collisionRatio+graphics.collisionRatio));
    }

    public void setSpeedX(double speedX) {
        this.speedX = speedX;
    }

    public void setSpeedY(double speedY) {
        this.speedY = speedY;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public void setCenterX(int centerX) {
        this.centerX = centerX;
    }

    public void setCenterY(int centerY) {
        this.centerY = centerY;
    }

    public double getAngle() {
        return angle;
    }

    public double getSpeedX() {
        return speedX;
    }

    public double getSpeedY() {
        return speedY;
    }

    public int getCenterX() {
        return centerX;
    }

    public int getCenterY() {
        return centerY;
    }
}

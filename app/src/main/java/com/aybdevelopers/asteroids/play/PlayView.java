package com.aybdevelopers.asteroids.play;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.PathShape;
import android.graphics.drawable.shapes.RectShape;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.aybdevelopers.asteroids.R;
import com.aybdevelopers.asteroids.preferences.SharedPreferencesAsteroids;

import java.util.List;
import java.util.Vector;

/**
 * Created by AYB on 09/10/2016.
 */

public class PlayView extends View implements SensorEventListener {

    private Context context;

    private Vector<Graphics> asteroids;
    private int asteroidsNumber = 5;
    private int asteroidsFragmentNumber = 3;

    private Graphics spaceShip;
    private int spaceShipRotation;
    private double spaceShipAceleration;
    private static final int STEP_ROTATION_SPACE_SHIP = 5;
    private static final double STEP_ACELERATION_SPACE_SHIP = 0.5f;


    private Vector<Graphics> misils;
    private static int STEP_SPEED_MISIL = 12;
    private Vector<Integer> timeMisil;

    private float mX = 0.0f;
    private float mY = 0.0f;
    private boolean shootMisil = false;

    private static final int SHOOT_RATE = 500;
    private long shootTime;

    private SharedPreferencesAsteroids mSharedPreferencesAsteroids;

    private GameThread gameThread;

    private SensorManager mSensorManager;

    private Drawable drawableSpaceShip;
    private Drawable drawableAsteroid;
    private Drawable drawableMisil;

    private SoundPool soundPool;
    private int idShoot;
    private int idExplosion;

    private Activity fatherActivity;

    public PlayView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        mSharedPreferencesAsteroids = new SharedPreferencesAsteroids(context);

        drawableAsteroid = getDrawableAsteroidsForGraphicType(mSharedPreferencesAsteroids.getGraphicType());
        drawableSpaceShip = getDrawableSpaceShipForGraphicType(mSharedPreferencesAsteroids.getGraphicType());
        if (drawableSpaceShip instanceof AnimationDrawable) {
            ((AnimationDrawable) drawableSpaceShip).start();
        }
        drawableMisil = getDrawableMisilForGraphicType(mSharedPreferencesAsteroids.getGraphicType());

        spaceShip = new Graphics(this, drawableSpaceShip);
        misils = new Vector<>();
        asteroids = new Vector<>();
        timeMisil = new Vector<>();
        for (int i = 0; i < asteroidsNumber; i++) {
            Graphics asteroid = new Graphics(this, drawableAsteroid);
            asteroid.setSpeedX(Math.random() * 4 - 2);
            asteroid.setSpeedY(Math.random() * 4 - 2);
            asteroid.setAngle((int) (Math.random() * 360));
            asteroid.setRotation((int) (Math.random() * 8 - 4));
            asteroids.add(asteroid);
        }

        soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        idShoot = soundPool.load(context, R.raw.shoot,0);
        idExplosion = soundPool.load(context, R.raw.explosion,0);

        gameThread = new GameThread(this, spaceShip, asteroids, misils);

        shootTime = System.currentTimeMillis();

    }

    public void activateSensor() {
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> listSensor = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
        if (!listSensor.isEmpty()) {
            Sensor orientationSensor = listSensor.get(0);
            mSensorManager.registerListener(this, orientationSensor, SensorManager.SENSOR_DELAY_GAME);
        }
    }

    public void desactivateSensor() {
        mSensorManager.unregisterListener(this);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        spaceShip.setCenterX(w / 2);
        spaceShip.setCenterY(h / 2);

        for (Graphics asteroid : asteroids) {
            do {
                asteroid.setCenterX((int) (Math.random() * w));
                asteroid.setCenterY((int) (Math.random() * h));
            } while (asteroid.getDistance(spaceShip) < (w + h) / 5);
        }

        if (gameThread.getState() == Thread.State.NEW) {
            gameThread.start();
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        synchronized (asteroids) {
            for (Graphics asteroid : asteroids) {
                asteroid.drawGraphic(canvas);
            }
        }
        spaceShip.drawGraphic(canvas);
        synchronized (misils) {
            for (Graphics misil : misils) {
                misil.drawGraphic(canvas);
            }
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mSharedPreferencesAsteroids.CONTROLLER_TOUCH.equals(mSharedPreferencesAsteroids.getControllerType())) {
            super.onTouchEvent(event);
            float x = event.getX();
            float y = event.getY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    shootMisil = true;
                    break;
                case MotionEvent.ACTION_MOVE:
                    float dx = Math.abs(x - mX);
                    float dy = Math.abs(y - mY);
                    if (dy < 6 && dx > 6) {
                        spaceShipAceleration = Math.round((x - mX) / 5);
                        if (spaceShipAceleration >= 0)
                            gameThread.setSpaceShipAcceleration(spaceShipAceleration);
                        shootMisil = false;
                    } else if (dx < 6 && dy > 6) {
                        spaceShipRotation = Math.round((y - mY) / 2);
                        gameThread.setSpaceShipRotation(spaceShipRotation);
                        shootMisil = false;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    spaceShipRotation = 0;
                    gameThread.setSpaceShipRotation(spaceShipRotation);
                    spaceShipAceleration = 0;
                    gameThread.setSpaceShipAcceleration(spaceShipAceleration);
                    if (shootMisil) {
                        activateMisil();
                    }
                    break;
            }
            mX = x;
            mY = y;
            return true;
        } else if (mSharedPreferencesAsteroids.CONTROLLER_ACCELEROMETER.equals(mSharedPreferencesAsteroids.getControllerType())) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    shootMisil = true;
                    if (shootMisil) {
                        activateMisil();
                    }
                    break;
            }
        }
        return false;
    }

    public void activateMisil() {
        long currentShootTime = System.currentTimeMillis();
        if (currentShootTime - shootTime > SHOOT_RATE) {
            Graphics misil = new Graphics(this, drawableMisil);
            misil.setCenterX(spaceShip.getCenterX());
            misil.setCenterY(spaceShip.getCenterY());
            misil.setAngle(spaceShip.getAngle());
            misil.setSpeedX(Math.cos(Math.toRadians(misil.getAngle())) * STEP_SPEED_MISIL);
            misil.setSpeedY(Math.sin(Math.toRadians(misil.getAngle())) * STEP_SPEED_MISIL);
            misils.add(misil);
            timeMisil.add((int) Math.min(this.getWidth() / Math.abs(misil.getSpeedX()), this.getHeight() / Math.abs(misil.getSpeedY())) - 2);
            shootTime = currentShootTime;
            soundPool.play(idShoot,1,1,1,0,1);
        }

    }

    private Drawable getDrawableAsteroidsForGraphicType(String graphic) {
        switch (graphic) {
            case SharedPreferencesAsteroids.GRAPHIC_3D:
                setLayerType(View.LAYER_TYPE_HARDWARE, null);
                return ResourcesCompat.getDrawable(getResources(), R.drawable.asteroid1, null);
            case SharedPreferencesAsteroids.GRAPHIC_BITMAP:
                setLayerType(View.LAYER_TYPE_HARDWARE, null);
                return ResourcesCompat.getDrawable(getResources(), R.drawable.asteroid1, null);
            case SharedPreferencesAsteroids.GRAPHIC_VECTOR:

                Path pathAsteroid = new Path();
                pathAsteroid.moveTo(0.3f, 0.0f);
                pathAsteroid.lineTo(0.6f, 0.0f);
                pathAsteroid.lineTo(0.6f, 0.3f);
                pathAsteroid.lineTo(0.8f, 0.2f);
                pathAsteroid.lineTo(1.0f, 0.4f);
                pathAsteroid.lineTo(0.8f, 0.6f);
                pathAsteroid.lineTo(0.9f, 0.9f);
                pathAsteroid.lineTo(0.8f, 1.0f);
                pathAsteroid.lineTo(0.4f, 1.0f);
                pathAsteroid.lineTo(0.0f, 0.6f);
                pathAsteroid.lineTo(0.0f, 0.2f);
                pathAsteroid.lineTo(0.3f, 0.0f);

                ShapeDrawable drawableAsteroids = new ShapeDrawable(new PathShape(pathAsteroid, 1, 1));
                drawableAsteroids.getPaint().setColor(Color.WHITE);
                drawableAsteroids.getPaint().setStyle(Paint.Style.STROKE);
                drawableAsteroids.setIntrinsicHeight(100);
                drawableAsteroids.setIntrinsicWidth(100);

                setBackgroundColor(Color.BLACK);
                setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                return drawableAsteroids;
            default:
                setLayerType(View.LAYER_TYPE_HARDWARE, null);
                return ResourcesCompat.getDrawable(getResources(), R.drawable.asteroid1, null);
        }
    }

    private Drawable getDrawableSpaceShipForGraphicType(String graphic) {
        ImageView spaceShipAnimated;
        switch (graphic) {
            case SharedPreferencesAsteroids.GRAPHIC_3D:
                spaceShipAnimated = new ImageView(context);
                spaceShipAnimated.setBackgroundResource(R.drawable.animation_space_ship);
                setLayerType(View.LAYER_TYPE_HARDWARE, null);
                return spaceShipAnimated.getBackground();
            case SharedPreferencesAsteroids.GRAPHIC_BITMAP:
                spaceShipAnimated = new ImageView(context);
                spaceShipAnimated.setBackgroundResource(R.drawable.animation_space_ship);
                setLayerType(View.LAYER_TYPE_HARDWARE, null);
                return spaceShipAnimated.getBackground();
            case SharedPreferencesAsteroids.GRAPHIC_VECTOR:

                Path pathAsteroid = new Path();
                pathAsteroid.moveTo(0.0f, 0.0f);
                pathAsteroid.lineTo(1.0f, 0.5f);
                pathAsteroid.lineTo(0.0f, 1.0f);
                pathAsteroid.lineTo(0.0f, 0.0f);

                ShapeDrawable drawableAsteroids = new ShapeDrawable(new PathShape(pathAsteroid, 1, 1));
                drawableAsteroids.getPaint().setColor(Color.WHITE);
                drawableAsteroids.getPaint().setStyle(Paint.Style.STROKE);
                drawableAsteroids.setIntrinsicHeight(40);
                drawableAsteroids.setIntrinsicWidth(55);

                setBackgroundColor(Color.BLACK);
                setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                return drawableAsteroids;
            default:
                setLayerType(View.LAYER_TYPE_HARDWARE, null);
                return ResourcesCompat.getDrawable(getResources(), R.drawable.space_ship_2, null);
        }
    }


    private Drawable getDrawableMisilForGraphicType(String graphic) {
        switch (graphic) {
            case SharedPreferencesAsteroids.GRAPHIC_3D:
                return ResourcesCompat.getDrawable(getResources(), R.drawable.misil, null);
            case SharedPreferencesAsteroids.GRAPHIC_BITMAP:
                return ResourcesCompat.getDrawable(getResources(), R.drawable.misil, null);
            case SharedPreferencesAsteroids.GRAPHIC_VECTOR:

                ShapeDrawable drawableMisil = new ShapeDrawable(new RectShape());
                drawableMisil.getPaint().setColor(Color.WHITE);
                drawableMisil.getPaint().setStyle(Paint.Style.STROKE);
                drawableMisil.setIntrinsicHeight(2);
                drawableMisil.setIntrinsicWidth(15);

                return drawableMisil;
            default:
                setLayerType(View.LAYER_TYPE_HARDWARE, null);
                return ResourcesCompat.getDrawable(getResources(), R.drawable.misil, null);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (mSharedPreferencesAsteroids.CONTROLLER_ACCELEROMETER.equals(mSharedPreferencesAsteroids.getControllerType())) {
            float valueX = event.values[0];
            float valueY = event.values[1];
            spaceShipRotation = (int) (valueX / 2);
            gameThread.setSpaceShipRotation(spaceShipRotation);
            spaceShipAceleration = (double) (valueY / 3);
            gameThread.setSpaceShipAcceleration(spaceShipAceleration);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mSharedPreferencesAsteroids.CONTROLLER_KEY_BOARD.equals(mSharedPreferencesAsteroids.getControllerType())) {
            super.onKeyDown(keyCode, event);
            boolean procesada = true;
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    spaceShipAceleration += STEP_ACELERATION_SPACE_SHIP;
                    break;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                    spaceShipRotation -= STEP_ROTATION_SPACE_SHIP;
                    break;
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    spaceShipRotation += STEP_ROTATION_SPACE_SHIP;
                    break;
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    activateMisil();
                    break;
                default:
                    procesada = false;
                    break;
            }
            return procesada;
        }
        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (mSharedPreferencesAsteroids.CONTROLLER_KEY_BOARD.equals(mSharedPreferencesAsteroids.getControllerType())) {
            super.onKeyDown(keyCode, event);
            boolean procesada = true;
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_UP:
                    spaceShipAceleration = 0;
                    break;
                case KeyEvent.KEYCODE_DPAD_LEFT:
                case KeyEvent.KEYCODE_DPAD_RIGHT:
                    spaceShipRotation = 0;
                    break;
                default:
                    procesada = false;
                    break;
            }
            return procesada;
        }
        return false;
    }

    public int getTimeMisilPosition(int i) {
        return timeMisil.get(i);
    }

    public void setTimeMisilPosition(int i, int timeMisil) {
        this.timeMisil.set(i, timeMisil);
    }

    public void removeTimeMisilPosition(int i) {
        this.timeMisil.remove(i);
    }

    public GameThread getGameThread() {
        return gameThread;
    }

    public int getIdExplosion() {
        return idExplosion;
    }

    public SoundPool getSoundPool() {
        return soundPool;
    }

    public void setFatherActivity(Activity fatherActivity){
        this.fatherActivity = fatherActivity;
    }

    public Activity getFatherActivity(){
        return fatherActivity;
    }
}

package com.aybdevelopers.asteroids.play;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

import com.aybdevelopers.asteroids.R;

import java.util.Vector;

/**
 * Created by AYB on 10/10/2016.
 */

public class GameThread extends Thread {

    private long lastProcess;
    private Graphics spaceShip;
    private Vector<Graphics> misils;
    private int spaceShipRotation;
    private double spaceShipAcceleration;
    private Vector<Graphics> asteroids;
    private PlayView playView;
    private int points = 0;

    private Activity fatherActivity;

    private boolean pause;
    private boolean running;

    private static final int PROCESS_PERIOD = 50;
    private static final int MAX_SPEED_SPACE_SHIP = 10;


    public GameThread(PlayView playView, Graphics spaceShip, Vector<Graphics> asteroids, Vector<Graphics> misils) {
        this.lastProcess = 0;
        this.spaceShip = spaceShip;
        this.asteroids = asteroids;
        this.misils = misils;
        this.playView = playView;
    }

    @Override
    public void run() {
        lastProcess = System.currentTimeMillis();
        running = true;
        while (running) {
            updatePhysics();
            synchronized (this){
                while(pause) {
                    try {
                        wait();
                    } catch (Exception e) {

                    }
                }
            }
        }
    }

    public synchronized  void pauseThread(){
        pause = true;
    }

    public synchronized  void restartThread(){
        pause = false;
        notify();
    }

    public void stopThread(){
        running = false;
        if(pause) restartThread();
    }

    private void updatePhysics() {
        long currentTime = System.currentTimeMillis();

        if (lastProcess + PROCESS_PERIOD > currentTime) {
            return;
        }

        double deltaTime = (currentTime - lastProcess) / PROCESS_PERIOD;
        lastProcess = currentTime;

        spaceShip.setAngle((int) (spaceShip.getAngle() + spaceShipRotation * deltaTime));
        double nSpeedX = spaceShip.getSpeedX() + spaceShipAcceleration * Math.cos(Math.toRadians(spaceShip.getAngle())) * deltaTime;
        double nSpeedY = spaceShip.getSpeedY() + spaceShipAcceleration * Math.sin(Math.toRadians(spaceShip.getAngle())) * deltaTime;
        if (Math.hypot(nSpeedX, nSpeedY) <= MAX_SPEED_SPACE_SHIP) {
            spaceShip.setSpeedX(nSpeedX);
            spaceShip.setSpeedY(nSpeedY);
        }
        spaceShip.incrementPosition(deltaTime);
        for (Graphics asteroid : asteroids) {
            asteroid.incrementPosition(deltaTime);
        }

        for (int i = 0; i < misils.size(); i++) {
            misils.get(i).incrementPosition(deltaTime);
            playView.setTimeMisilPosition(i, playView.getTimeMisilPosition(i) - (int) deltaTime);
            if (playView.getTimeMisilPosition(i) < 0) {
                misils.remove(i);
                playView.removeTimeMisilPosition(i);
            } else {
                for (int j = 0; j < asteroids.size(); j++) {
                    if (misils.get(i).onCollisionEnter(asteroids.elementAt(j))) {
                        destroyAsteroid(j);
                        destroyMisil(i);
                        break;
                    }
                }
            }
        }

        for(Graphics asteroid:asteroids){
            if(asteroid.onCollisionEnter(spaceShip)){
                exit();
            }
        }
    }

    private void destroyAsteroid(int i) {
        synchronized (asteroids) {
            asteroids.removeElementAt(i);
            playView.getSoundPool().play(playView.getIdExplosion(),1,1,0,0,1);
            points += 1000;
        }
        if(asteroids.isEmpty()){
            exit();
        }
    }

    private void destroyMisil(int i){
        synchronized (misils){
            misils.remove(i);
            playView.removeTimeMisilPosition(i);
        }
    }

    public void setSpaceShipAcceleration(double spaceShipAcceleration) {
        this.spaceShipAcceleration = spaceShipAcceleration;
    }

    public void setSpaceShipRotation(int spaceShipRotation) {
        this.spaceShipRotation = spaceShipRotation;
    }

    private void exit(){
        Bundle bundle = new Bundle();
        bundle.putInt(PlayActivity.EXTRA_POINTS, points);
        Intent intent = new Intent();
        intent.putExtras(bundle);
        fatherActivity = playView.getFatherActivity();
        fatherActivity.setResult(Activity.RESULT_OK, intent);
        fatherActivity.finish();
    }

}

package com.aybdevelopers.asteroids.play;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.aybdevelopers.asteroids.R;
import com.aybdevelopers.asteroids.preferences.SharedPreferencesAsteroids;

public class PlayActivity extends AppCompatActivity {

    private PlayView playView;
    private MediaPlayer mediaPlayer;
    private SharedPreferencesAsteroids mSharedPreferencesAsteroids;

    public static final String EXTRA_POINTS = "points";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        mSharedPreferencesAsteroids = new SharedPreferencesAsteroids(this);

        playView = (PlayView) findViewById(R.id.game_view);
        playView.setFatherActivity(this);
        mediaPlayer = MediaPlayer.create(this, R.raw.background_audio);
        mediaPlayer.setLooping(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        playView.getGameThread().pauseThread();
        playView.desactivateSensor();
        if(mSharedPreferencesAsteroids.getMusicActive()){
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        playView.getGameThread().restartThread();
        playView.activateSensor();
        if(mSharedPreferencesAsteroids.getMusicActive()){
            mediaPlayer.start();
        }
        playView.setFatherActivity(this);
    }

    @Override
    protected void onDestroy() {
        playView.getGameThread().stopThread();
        super.onDestroy();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }
}

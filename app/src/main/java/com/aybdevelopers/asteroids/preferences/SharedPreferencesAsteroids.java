package com.aybdevelopers.asteroids.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by AYB on 09/10/2016.
 */

public class SharedPreferencesAsteroids {

    public static final String MUSIC = "music";
    public static final String GRAPHIC = "graphic";
    public static final String FRAGMENTS = "fragments";
    public static final String CONTROLLER = "controller";
    public static final String STORE = "store";

    public static final String GRAPHIC_VECTOR = "0";
    public static final String GRAPHIC_BITMAP = "1";
    public static final String GRAPHIC_3D = "2";

    public static final String CONTROLLER_ACCELEROMETER = "0";
    public static final String CONTROLLER_TOUCH = "1";
    public static final String CONTROLLER_KEY_BOARD = "2";

    public static final String STORE_PREFERENCES = "0";
    public static final String STORE_FILE = "1";
    public static final String STORE_FILE_EXTERNAL = "2";
    public static final String STORE_FILE_RAW = "3";
    public static final String STORE_FILE_ASSETS = "4";
    public static final String STORE_FILE_XML_SAX = "5";
    public static final String STORE_DATABASE_LOCAL = "6";
    public static final String STORE_SOCKET= "7";
    public static final String STORE_WEB_SERVICE= "8";

    private SharedPreferences mSharedPreferences;

    public SharedPreferencesAsteroids(Context context) {
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public String getGraphicType() {
        return mSharedPreferences.getString(GRAPHIC, "-1");
    }

    public String getControllerType(){
        return mSharedPreferences.getString(CONTROLLER, "-1");
    }

    public boolean getMusicActive(){
        return mSharedPreferences.getBoolean(MUSIC, true);
    }

    public String getStoreType(){
        return mSharedPreferences.getString(STORE, "-1");
    }
}

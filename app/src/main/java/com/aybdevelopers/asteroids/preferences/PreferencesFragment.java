package com.aybdevelopers.asteroids.preferences;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.aybdevelopers.asteroids.R;

/**
 * Created by AYB on 08/10/2016.
 */

public class PreferencesFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
